from django.core.management.base import BaseCommand
from app.models import User, Document
from django.conf import settings

class Command(BaseCommand):
    help = 'init'
    def handle(self, *args, **kwargs):
        GIFT_CODE = '!~~Sup3r Prem1um G1ft C0DE~~!'
        import os
        if not os.path.exists('{}db'.format(settings.BASE_DIR)):
            os.mkdir('{}/db'.format(settings.BASE_DIR))
        os.system('rm db/db.sqlite3')
        os.system('python "{}"/manage.py makemigrations'.format(settings.BASE_DIR))
        os.system('python "{}"/manage.py migrate --run-syncdb'.format(settings.BASE_DIR))
        if User.objects.all():
            return
        admin = User.objects.create_user(username='admin', password='hacker101', is_premium=False, is_admin=True, gift_code=GIFT_CODE)
        guest = User.objects.create_user(username='guest', password='pass', is_premium=False)
        