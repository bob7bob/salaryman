#!/bin/bash
rm -rf doc
mkdir doc
cp $1 doc
cd doc
unzip $1 
cp ../document.xml word
zip ../$2 ./* -r
cd ..